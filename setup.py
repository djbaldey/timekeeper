#
# Copyright (c) 2021, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from setuptools import setup

# Dynamically calculate the version based on timekeeper.VERSION.
version = __import__('timekeeper').get_version()

with open('README.rst', 'r') as f:
    long_description = f.read()

with open('requirements.txt') as f:
    requirements = [
        l.split('#', 1)[0].strip() for l in f.read().splitlines()
        if not l.strip().startswith('#')
    ]

setup(
    name='timekeeper',
    version=version,
    description=(
        'GUI application for logging a work on user projects.'
    ),
    long_description=long_description,
    author='Grigoriy Kramarenko',
    author_email='root@rosix.ru',
    url='https://gitlab.com/djbaldey/timekeeper/',
    license='BSD License',
    platforms='any',
    zip_safe=False,
    packages=['timekeeper'],
    scripts=[
        # 'scripts/timekeeper',
    ],
    include_package_data=True,
    install_requires=requirements,
    classifiers=[
        # List of Classifiers: https://pypi.org/classifiers/
        'Development Status :: 1 - Planning',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: POSIX',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
)
