#
# Copyright (c) 2021, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from datetime import datetime, timedelta, timezone
from slugify import slugify

utc = timezone(timedelta(0), name='UTC')


def now():
    return datetime.utcnow().replace(tzinfo=utc)


def clean_project_name(text):
    value = slugify(str(text))
    if not value:
        raise ValueError('The name %r became empty after processing.' % text)
    return value
