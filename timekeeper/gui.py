#
# Copyright (c) 2021, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import gi
gi.require_version('Gtk', '3.0')  # NOQA
from gi.repository import Gtk

from timekeeper.db import get_settings, current_project


class App(object):

    def __init__(self, **kwargs):

        settings = get_settings()

        self.window = window = Gtk.Window()
        window.connect_after('destroy', self.destroy)

        width = settings.get('window_width', 800)
        heigth = settings.get('window_heigth', 600)
        window.resize(width, heigth)
        if settings.get('fullscreen'):
            window.fullscreen()
        else:
            window.set_position(Gtk.WindowPosition.CENTER)
            window.set_keep_above(True)

        self.change_title()
        # scroller = Gtk.ScrolledWindow()
        # scroller.add()
        # window.add(scroller)
        window.show_all()

    def change_title(self):
        project = current_project()
        if project:
            title = project.get('title')
            self.window.set_title(title)

    def create(self):
        Gtk.main()

    def destroy(self, window=None):
        Gtk.main_quit()


def main(*args, **kwargs):
    app = App(*args, **kwargs)
    app.create()
