#
# Copyright (c) 2021, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from os import path, makedirs


CONFIG_DIR = path.expanduser(path.join('~', '.config', 'timekeeper'))
if not path.exists(CONFIG_DIR):
    makedirs(CONFIG_DIR)


def path_to_database(dbname):
    # Shelve auto added `.db` for file.
    return path.join(CONFIG_DIR, dbname)
