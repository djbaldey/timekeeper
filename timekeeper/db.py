#
# Copyright (c) 2021, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#

import os
import shelve

from timekeeper.conf import path_to_database
from timekeeper.utils import now


def get_settings():
    with shelve.open(path_to_database('settings')) as db:
        return dict(db.items())


def update_settings(**kwargs):
    with shelve.open(path_to_database('settings')) as db:
        db.update(kwargs)


def get_project(name, with_log=False):
    with shelve.open(path_to_database('projects')) as db:
        project = db.get(name)
    if project and with_log:
        with shelve.open(path_to_database('log_%s' % name)) as db:
            project['log'] = db.get('log', [])
    return project


def update_project(name, log=None, **kwargs):
    with shelve.open(path_to_database('projects')) as db:
        project = db.get(name, {})
        project.update(kwargs)
        db[name] = project
    if log and isinstance(log, list):
        with shelve.open(path_to_database('log_%s' % name)) as db:
            db['log'] = log
    return project


create_project = update_project


def delete_project(name, with_log=False):
    assert current_work().get(name) != name
    with shelve.open(path_to_database('projects')) as db:
        if name in db:
            del db[name]
    if with_log:
        path = path_to_database('log_%s' % name)
        if os.path.exists(path):
            os.remove(path)


def current_work():
    with shelve.open(path_to_database('current')) as db:
        return dict(db.items())


def current_project():
    work = current_work()
    if work:
        return get_project(work.get('name'), True)


def start_work(project_name):
    value = now().isoformat()
    with shelve.open(path_to_database('current')) as db:
        assert 'name' not in db or db.get('finished'), (
            'The current work is not finished yet.')
        for k, v in db.items():
            del db[k]
        db['name'] = project_name
        db['started'] = value
    with shelve.open(path_to_database('log_%s' % project_name)) as db:
        log = db.get('log', [])
        log.append({'started': value})
        db['log'] = log
    return value


def _set_values_to_lastlog(project_name, **kwargs):
    with shelve.open(path_to_database('log_%s' % project_name)) as db:
        log = db.get('log', [{}])
        last = log[-1]
        for key, value in kwargs.items():
            last[key] = value
        db['log'] = log


def _set_values_to_current(project_name=None, **kwargs):
    with shelve.open(path_to_database('current')) as db:
        if project_name:
            assert 'name' in db, 'Current work is not defined.'
            assert db['name'] == project_name, 'Different work is running.'
        else:
            project_name = db.get('name')
        finished = db.get('finished')
        if finished or not project_name:
            return
        for key, value in kwargs.items():
            db[key] = value
    return project_name


def finish_work(project_name, comment=''):
    value = now().isoformat()
    with shelve.open(path_to_database('current')) as db:
        assert 'name' in db and not db.get('finished'), (
            'The current work is not found or already finished.')
        db['name'] = project_name
        db['updated'] = value
        db['finished'] = value
        db['comment'] = comment
    _set_values_to_lastlog(
        project_name, updated=value, finished=value, comment=comment)
    return value


def update_work():
    value = now().isoformat()
    project_name = _set_values_to_current(updated=value)
    if project_name:
        _set_values_to_lastlog(project_name, updated=value)
        return value


def correct_work(minutes):
    assert isinstance(minutes, int)
    value = current_work().get('correction', 0) + minutes
    project_name = _set_values_to_current(correction=value)
    if project_name:
        _set_values_to_lastlog(project_name, correction=value)
        return value
